PWMSTAT
=========================================

#Compiling

Use the provided ```cmake``` file or run:

```g++ -O3 pwmstat.cpp -o pwmstat```

#Usage

Display help with option ```-h```.

Here is an example, provided the program is in ```code/```:

```./pwmstat -s ../data/subtilis-chip-noc.fasta -m ../data/noc.pwm -n 10000 -o res-noc_10000```
