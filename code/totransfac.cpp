#include <iostream>
#include <fstream>
#include <exception>
#include <cstring>
#include <cmath>
#include <sstream>
#include "readinput.h"
#include <cstddef>

using namespace std;

void displayHelp();

int main(int argc, char **argv) {
	
	//////////////////////////////////////////////////////////////////////////////////
	// Gestion des arguments
	char *pwmFilename, *outputFilename;
	bool pwmSet=false, outputSet=false;
	int num_arg=1;
	while (num_arg < argc) {
		if ( (strcmp (argv[num_arg], "-m") == 0) && (num_arg + 1 < argc) ) {
			num_arg++;
			pwmFilename=argv[num_arg];
			num_arg++;
			pwmSet=true;
		} else if ( (strcmp (argv[num_arg], "-o") == 0) && (num_arg + 1 < argc) ) {
			num_arg++;
			outputFilename=argv[num_arg];
			num_arg++;
			outputSet=true;
		} else {
			displayHelp();
			return 0;
		}
    }
	if( !(pwmSet && outputSet) ) {
		displayHelp();
		return 0;
	}
	
	try {
		
		// taille de la matrice poids-position
		int pwmLength;
		
		// matrice poids-position
		vector< vector<double> > pwm;
		
		readMatrix(pwmFilename, pwmLength, pwm);
		
		// fichier de sortie
		ofstream output(outputFilename);
		if (!output.is_open()) throw(string("Can't open output file."));
		
		output<<"# File created with totransfac."<<endl<<endl;
		output<<"ttfc\tA\tC\tG\tT"<<endl;
		for(int j=0;j<pwmLength;j++) {
			output<<j+1;
			for(int i=0;i<4;i++) {
				output<<"\t"<<pwm[i][j]*100.0;
			}
			output<<endl;
		}
		
		output.close();
		
	}
	// Différents types d'erreur traitées
	catch (exception &e) {
		cout<<e.what()<<endl;
		displayHelp();
	}
	catch (string &s) {
		cout<<s<<endl;
		displayHelp();
	}
	catch (...) {
		cout<<"Unknown error."<<endl;
		displayHelp();
	}
	
	return EXIT_SUCCESS;
}

void displayHelp() {
	cout<<endl<<"Old format to Transfac:"<<endl;
	cout<<endl<<"\tFor a pwm given in old format,"<<endl;
	cout<<"\tgives a Transfac format similar to that used by BioProspector."<<endl;
	cout<<endl<<"\tSyntax: totransfac -m <filename> -o <filename>"<<endl;
	cout<<endl<<"\tArguments:"<<endl;
	cout<<"\t\t-m PWM file"<<endl;
	cout<<"\t\t-o Output file"<<endl<<endl;
}