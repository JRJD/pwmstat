#include <string>
#include <vector>
#include <map>

using namespace std;

vector<long> power; // puissances de 4 pour accélérer les calculs

class GenomeMarkovChain {
public:
	virtual double joint(const int &a, const int &b)=0;
	virtual double forwardConditional(const int &a, const int &b)=0;
	virtual double backwardConditional(const int &a, const int &b)=0;
	virtual double marginal(const int &a)=0;
};

class GenomeMarkov1 : public GenomeMarkovChain {
	vector<vector<double> > jointTable,forwardConditionalTable,backwardConditionalTable;
	vector<double> marginalTable;
	
public:
	GenomeMarkov1(const vector<vector<double> > &jointTable) {
		this->jointTable=vector<vector<double> >(jointTable);
		
		double marg=0;
		for(int i=0;i<4;i++)
			for(int j=0;j<4;j++)
				marg+=jointTable[i][j];
		for(int i=0;i<4;i++)
			for(int j=0;j<4;j++) this->jointTable[i][j]/=marg;
		
		this->forwardConditionalTable=vector<vector<double> >(this->jointTable);
		this->backwardConditionalTable=vector<vector<double> >(this->jointTable);
		this->marginalTable=vector<double>(4,0.0);
		
		for(int i=0;i<4;i++) {
			double margfor=0, margback=0;
			for(int j=0;j<4;j++) {
				margfor+=this->jointTable[i][j];
				margback+=this->jointTable[j][i];
			}
			this->marginalTable[i]=(margfor+margback)/2;
			for(int j=0;j<4;j++) {
				this->forwardConditionalTable[i][j]/=margfor;
				this->backwardConditionalTable[j][i]/=margback;
			}
		}
	}
	
	double joint(const int &a, const int &b) {return jointTable[a][b];}
	double forwardConditional(const int &a, const int &b) {return forwardConditionalTable[a][b];}
	double backwardConditional(const int &a, const int &b) {return backwardConditionalTable[a][b];}
	double marginal(const int &a) {return marginalTable[a];}
	
	double consistencyCheck() {
		vector<vector<double> > testForward(this->forwardConditionalTable);
		double errorForward=0, errorBackward=0;
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				errorForward+=(this->forwardConditionalTable[i][j]*this->marginalTable[i]-this->jointTable[i][j])*(this->forwardConditionalTable[i][j]*this->marginalTable[i]-this->jointTable[i][j]);
				errorBackward+=(this->backwardConditionalTable[i][j]*this->marginalTable[j]-this->jointTable[i][j])*(this->backwardConditionalTable[i][j]*this->marginalTable[j]-this->jointTable[i][j]);
			}
		}
		errorForward/=16;
		errorBackward/=16;
		errorForward=sqrt(errorForward);
		errorBackward=sqrt(errorBackward);
		return errorForward+errorBackward;
	}
};

class GenomeMarkov0 : public GenomeMarkovChain {
	vector<double> marginalTable;
	
public:
	GenomeMarkov0(vector<double> &marginalTable) {
		this->marginalTable=vector<double>(marginalTable);
		
		double marg=0;
		for(int i=0;i<4;i++) marg+=marginalTable[i];
		for(int i=0;i<4;i++) this->marginalTable[i]/=marg;
	}
	
	double joint(const int &a, const int &b) {return marginalTable[a]*marginalTable[b];}
	double forwardConditional(const int &a, const int &b) {return marginalTable[b];}
	double backwardConditional(const int &a, const int &b) {return marginalTable[a];}
	double marginal(const int &a) {return marginalTable[a];}
};

class HitInformation {			// Conteneur d'information sur un hit donné
public:
	double likelihood;			// La vraisemblance du hit
	map<long,double> overlaps;	// Un dictionnaire des recouvrements suivants : clé : le recouvrement en base 4 ; valeur : la probabilité de ce recouvrement
	// Attention : c'est la probabilité que ce hit soit le premier hit suivant (et non pas sa probabilité absolue)
	// --> Un hit d'ordre supérieur au premier hit suivant sera compté comme fils de ce premier hit suivant
	double occurrenceProbability;	// La probabilité qu'un paquet de ce type commence (pas de recouvrement par la gauche * probabilité d'apparition)
	
	HitInformation(double likelihood, map<long,double> overlaps, double occurrenceProbability) {
		this->likelihood=likelihood;
		this->overlaps=overlaps;
		this->occurrenceProbability=occurrenceProbability;
	}
};

void setPower(int l) {
	long aux=1;
	for(int i=0;i<=l;i++) {
		power.push_back(aux);
		aux*=4;
	}
}

bool isPartialHit(const vector< vector<double> > &pwm, const int &start, const int &length, long word) {
	for(int j=start;j<length;j++) {
		if(pwm[j][word%4]==0.0)
			return false;
		word/=4;
	}
	return true;
}

void recHits(map<long,double> &hits, const vector< vector<double> > &pwm, const int &pwmLength, int position, double currentLikelihood, long currentSequence) {
	if(position==pwmLength) {
		hits.insert(pair<long,double>(currentSequence,currentLikelihood));
	} else {
		for(int i=0;i<4;i++) {
			double p=pwm[position][i];
			if(p!=0.0)
				recHits(hits,pwm,pwmLength,position+1,currentLikelihood*p,currentSequence+i*power[position]);
		}
	}
}

void recRightOverlaps(const vector< vector<double> > &pwm, const int &pwmLength, GenomeMarkovChain* &genomeProbability, HitInformation &hitInfo, const long &hitSequence, const int &order, long currentWord, double currentProbability, int position) {
	if(position==pwmLength) {
		long concat=(hitSequence%power[order])+power[order]*currentWord;
		for(int i=1;i<order;i++) {
			concat/=4;
			if(isPartialHit(pwm,0,pwmLength,concat%power[pwmLength]))
				return;
		}
		hitInfo.overlaps[currentWord]+=currentProbability;
	} else {
		int previousLetter=(currentWord/power[position-1])%4;
		for(int i=0;i<4;i++) {
			double p=pwm[position][i];
			if(p!=0.0)
				recRightOverlaps(pwm,pwmLength,genomeProbability,hitInfo,hitSequence,order,currentWord+i*power[position],currentProbability*genomeProbability->forwardConditional(previousLetter,i),position+1);
		}
	}
}

void recLeftOverlaps(double &probabilitySum, const vector< vector<double> > &pwm, const int &pwmLength, GenomeMarkovChain* &genomeProbability, const long &hitSequence, const int &order, long currentWord, double currentProbability, int position) {
	if(position==-1) {
		long concat=(hitSequence/power[order])*power[pwmLength]+(currentWord%power[pwmLength]);
		for(int i=1;i<pwmLength-order;i++) {
			concat/=4;
			if(isPartialHit(pwm,0,pwmLength,concat%power[pwmLength]))
				return;
		}
		probabilitySum+=currentProbability;
	} else {
		int previousLetter=currentWord%4;
		for(int i=0;i<4;i++) {
			double p=pwm[position][i];
			if(p!=0.0)
				recLeftOverlaps(probabilitySum,pwm,pwmLength,genomeProbability,hitSequence,order,4*currentWord+i,currentProbability*genomeProbability->backwardConditional(i,previousLetter),position-1);
		}
	}
}

double cdf(double x)
{
    // constants
    double a1 =  0.254829592;
    double a2 = -0.284496736;
    double a3 =  1.421413741;
    double a4 = -1.453152027;
    double a5 =  1.061405429;
    double p  =  0.3275911;
	
    // Save the sign of x
    int sign = 1;
    if (x < 0)
        sign = -1;
    x = fabs(x)/sqrt(2.0);
	
    // A&S formula 7.1.26
    double t = 1.0/(1.0 + p*x);
    double y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);
	
    return 0.5*(1.0 + sign*y);
}
