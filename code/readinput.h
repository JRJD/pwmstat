#include <fstream>
#include <vector>
#include "pwminfo.h"

using namespace std;

void readMatrix(const char *filename, int &pwmLength, vector< vector<double> > &pwm) { // deprecated
	ifstream pwmFile(filename);
	if (!pwmFile.is_open()) throw(string("Can't open PWM file."));
	// La première entrée du fichier est la longueur de la PWM
	pwmFile>>pwmLength;
	for(int i=0;i<4;i++) {
		vector<double> row;
		for(int j=0;j<pwmLength;j++) {
			double f;
			pwmFile>>f;
			row.push_back(f);
		}
		pwm.push_back(row);
	}
	pwmFile.close();
}

int letter_to_int(char c) {
	int result;
	switch (c) {
		case 'A':
		case 'a':
			result=0;
			break;
		case 'C':
		case 'c':
			result=1;
			break;
		case 'G':
		case 'g':
			result=2;
			break;
		case 'T':
		case 't':
			result=3;
			break;
	}
	return result;
}

char int_to_letter(int c) {
	int result;
	switch (c) {
		case 0:
			result='A';
			break;
		case 1:
			result='C';
			break;
		case 2:
			result='G';
			break;
		case 3:
			result='T';
			break;
	}
	return result;
}

void readGenomeM0(const char *filename, const vector< vector<double> > &pwm, double &likelihoodSum, int &genomeLength, GenomeMarkovChain* &genomeProbability) {
	int pwmLength=pwm.size();
	ifstream sequenceFile(filename);
	if (!sequenceFile.is_open()) throw(string("Can't open sequence file."));
	
	string sequence_string;
	
	vector<double> lettersProbability(4,0);
	
	// On utilise comme cadre de lecture un vecteur circulaire nommé 'frame',
	// de longueur 'pwmLength' et qui commence à 'frame_start'.
	vector<int> frame;
	
	// Entier indiquant l'indice de début du vecteur circulaire
	int frame_start=0;
	// Compte le nombre de bases traitées
	int counter=0;
	// L'algorithme tourne tant qu'il reste un caractère dans le fichier
	while(getline(sequenceFile,sequence_string)) {
		stringstream sequence(sequence_string);
		bool skip_line=false;
		while(sequence.good()) {
			// On décale le cadre de lecture et on insère le prochain caractère.
			char c;
			do {
				if(!sequence.good()) {
					skip_line=true;
					break;
				}
				sequence>>c;
				if(!sequence.good()) { // nécessaire pour ne pas compter
					skip_line=true;    // deux fois la dernière lettre 
					break;             // avant un saut de ligne
				}                      // Un mystère informatique...
				if(c=='>') {
					skip_line=true;
					break;
				}
			} while(c!='A' && c!='C' && c!='G' && c!='T' && c!='a' && c!='c' && c!='g' && c!='t');
			if(skip_line)
				break;
			int letter=letter_to_int(c);
			lettersProbability[letter]+=1;
			if(counter>=pwmLength) {
				frame[frame_start]=letter;
				frame_start=(frame_start+1)%pwmLength;
			} else {
				frame.push_back(letter);
			}
			counter++;
			if(counter>=pwmLength) {
				float likelihood=1.0;
				// On parcourt le cadre de lecture
				for(int i=0;i<pwmLength;i++) {
					float probability=pwm[i][frame[(frame_start+i)%pwmLength]];
					// Si la probabilité correspondant au caractère en cette position est nulle,
					// pas la peine de continuer, on avorte la boucle for.
					if(probability==0.0) {
						likelihood=0.0;
						break;
					}
					likelihood*=probability;
				}
				
				// On ajoute l'adéquation lue
				if(likelihood!=0.0) likelihoodSum+=likelihood;
			}
		}
	}
	sequenceFile.close();
	
	for(int i=0;i<4;i++) genomeLength+=lettersProbability[i];
	genomeProbability=new GenomeMarkov0(lettersProbability);
}

void readGenomeM1(const char *filename, const vector< vector<double> > &pwm, double &likelihoodSum, int &genomeLength, GenomeMarkovChain* &genomeProbability) {
	int pwmLength=pwm.size();
	ifstream sequenceFile(filename);
	if (!sequenceFile.is_open()) throw(string("Can't open sequence file."));
	
	string sequence_string;
	
	vector<vector<double> > lettersProbability;
	for(int i=0;i<4;i++) lettersProbability.push_back(vector<double>(4,0));
	
	// On utilise comme cadre de lecture un vecteur circulaire nommé 'frame',
	// de longueur 'pwmLength' et qui commence à 'frame_start'.
	vector<int> frame;
	
	// Entier indiquant l'indice de début du vecteur circulaire
	int frame_start=0;
	// Compte le nombre de bases traitées
	int counter=0;
	int lastLetter=-1;
	// L'algorithme tourne tant qu'il reste un caractère dans le fichier
	while(getline(sequenceFile,sequence_string)) {
		stringstream sequence(sequence_string);
		bool skip_line=false;
		while(sequence.good()) {
			// On décale le cadre de lecture et on insère le prochain caractère.
			char c;
			do {
				if(!sequence.good()) {
					skip_line=true;
					break;
				}
				sequence>>c;
				if(!sequence.good()) { // nécessaire pour ne pas compter
					skip_line=true;    // deux fois la dernière lettre 
					break;             // avant un saut de ligne
				}                      // Un mystère informatique...
				if(c=='>') {
					skip_line=true;
					break;
				}
			} while(c!='A' && c!='C' && c!='G' && c!='T' && c!='a' && c!='c' && c!='g' && c!='t');
			if(skip_line)
				break;
			int letter=letter_to_int(c);
			if(lastLetter!=-1) lettersProbability[lastLetter][letter]+=1;
			lastLetter=letter;
			if(counter>=pwmLength) {
				frame[frame_start]=letter;
				frame_start=(frame_start+1)%pwmLength;
			} else {
				frame.push_back(letter);
			}
			counter++;
			if(counter>=pwmLength) {
				float likelihood=1.0;
				// On parcourt le cadre de lecture
				for(int i=0;i<pwmLength;i++) {
					float probability=pwm[i][frame[(frame_start+i)%pwmLength]];
					// Si la probabilité correspondant au caractère en cette position est nulle,
					// pas la peine de continuer, on avorte la boucle for.
					if(probability==0.0) {
						likelihood=0.0;
						break;
					}
					likelihood*=probability;
				}
				
				// On ajoute l'adéquation lue
				if(likelihood!=0.0) likelihoodSum+=likelihood;
			}
		}
	}
	sequenceFile.close();
	
	genomeLength=counter;
	genomeProbability=new GenomeMarkov1(lettersProbability);
		
}

string& trim_right_inplace(string& s, const string& delimiters = " \f\n\r\t\v" ) {
	return s.erase( s.find_last_not_of( delimiters ) + 1 );
}

string& trim_left_inplace(string& s, const string& delimiters = " \f\n\r\t\v" ) {
	return s.erase( 0, s.find_first_not_of( delimiters ) );
}

string& trim(string& s, const string& delimiters = " \f\n\r\t\v" ) {
	return trim_left_inplace( trim_right_inplace( s, delimiters ), delimiters );
}

enum empties_t { empties_ok, no_empties };

template <typename Container>
Container& split(
				 Container&                            result,
				 const typename Container::value_type& s,
				 const typename Container::value_type& delimiters,
				 empties_t                      empties)
{
	result.clear();
	size_t current;
	size_t next = -1;
	do
	{
		if (empties == no_empties)
		{
			next = s.find_first_not_of( delimiters, next + 1 );
			if (next == Container::value_type::npos) break;
			next -= 1;
		}
		current = next + 1;
		next = s.find_first_of( delimiters, current );
		result.push_back( s.substr( current, next - current ) );
	}
	while (next != Container::value_type::npos);
	return result;
}

void readTransfacMatrix(const char *filename, vector< vector<double> > &pwm) {
	ifstream pwmFile(filename);
	if (!pwmFile.is_open()) throw(string("Can't open PWM file."));
	bool started=false;
	string line;
	while(getline(pwmFile,line)) {
		trim(line);
		if(line[0]=='#' || line=="") continue;
		if(!started) {
			started=true;
			continue;
		}
		stringstream line_stream(line);
		if(!line_stream.good()) throw(string("Bad Transfac format for PWM file"));
		int n;
		line_stream>>n;
		vector<double> row;
		double f;
		for(int i=0;i<4;i++) {
			if(!line_stream.good()) throw(string("Bad Transfac format for PWM file"));
			line_stream>>f;
			row.push_back(f/100.0);
		}
		pwm.push_back(row);
	}
	pwmFile.close();
}
