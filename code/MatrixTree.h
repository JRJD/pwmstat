#include <istream>
#include <fstream>
#include <map>
#include <string>
#include <stdlib.h>
#include <time.h>
#include "Hit.h"

using namespace std;

inline int poisson(double lambda) {
	int n=-1;
	double s=0;
	do {
		n++;
		double u=(double)rand()/(double)RAND_MAX;
		s=s-log(u)/lambda;
	} while(s<=1);
	return n;
}

class MatrixTree {
	
	map<long,Hit> hits;
	
	int generateNumberOfPackets(long sequenceLength, map<long,Hit>::iterator packetType, bool enhanced=false);
	
	double generatePacketLikelihood(HitPointer hit) {
		double sum=0.0;
		while(hit!=hits.end())
			hit=hit->second.descendInTree(sum,(double)rand()/(double)RAND_MAX);
		return sum;
	}
	
	double lastLogEnhancementWeight;
	
public:
	
	MatrixTree(istream &input) throw(string);
	double generateSequenceLikelihood(long sequenceLength, bool enhanced=false);
	double getLastEnhancementWeight() {/*cout<<exp(lastLogEnhancementWeight)<<endl;*/ return exp(lastLogEnhancementWeight);}
	void initializeForEnhancedSimulation(long sequenceLength, double threshold, int iteration);
	
};

MatrixTree::MatrixTree(istream &input) throw(string) {
	
	int pwmLength;
		
	// récupération de la taille de la matrice
	input>>pwmLength;
	// itérateur qui parcourera les mots
	// lors de la lecture du fichier
	map<long,Hit>::iterator currentHit;
	// pour chaque noeud, on cumule les probas des branches,
	// en vue de la recherche dichotomique.
	double probabilitySum;
		
	while(input.good()) {
			
		// chaque ligne commence par '+' ou '-',
		// indiquant si l'on change de mot ou non
		char mode;
		input>>mode;
			
		if(!input.good())break;
			
		if(mode=='+') { // un nouveau mot commence
				
			// récupération des informations autres 
			// que les recouvrements
			long sequence;
			double likelihood;
			double occurrenceProbability;
			input>>sequence;
			input>>mode;
			input>>likelihood;
			input>>mode;
			input>>occurrenceProbability;
				
			// ajout des informations à la table
			hits.insert(pair<long,Hit>(sequence, Hit(likelihood,occurrenceProbability) ));
				
			// mise à jour de l'itérateur
			// il pointe maintenant sur le mot
			// qui vient d'être lu.
			currentHit=hits.find(sequence);
				
			// remise à zéro de la somme,
			// un nouveau noeud commence
			probabilitySum=0.0;
				
		} else { // un recouvrement (la ligne commence par'-')
				
			// récupération des informations
			long sequence;
			double probability;
			input>>sequence;
			input>>mode;
			input>>probability;
				
			probabilitySum+=probability; // on veut les probabilités cumulées
				
			// ajout à la table des recouvrements
			// contenue dans l'objet Hit_Information
			// de currentHit
			currentHit->second.overlaps.push_back(pair<double,long>(probabilitySum,sequence));
		}
	}
	
	// remplissage de la map d'iterator de Hit
	for(map<long,Hit>::iterator hit=hits.begin();hit!=hits.end();hit++) {
		for(vector<pair<double, long> >::iterator overlap=hit->second.overlaps.begin();overlap!=hit->second.overlaps.end();overlap++) {
			hit->second.overlapsPointers.push_back( pair<double,HitPointer>(overlap->first,hits.find(overlap->second)) );
		}
		// le cas où on s'arrêtera dans la descente de l'arbre
		hit->second.overlapsPointers.push_back( pair<double,HitPointer>(1.0,hits.end()) );
		// hit->second.overlaps.clear();
	}
	
	// démarrage du générateur de nombres aléatoires
	srand((unsigned)time(0));
}

inline int MatrixTree::generateNumberOfPackets(long sequenceLength, map<long,Hit>::iterator packetType, bool enhanced) {
	if(!enhanced)
		return poisson(sequenceLength*packetType->second.occurrenceProbability);
	
	int N=poisson(sequenceLength*packetType->second.enhancedOccurrenceProbability);
	lastLogEnhancementWeight+=sequenceLength*(packetType->second.enhancedOccurrenceProbability-packetType->second.occurrenceProbability)+N*log(packetType->second.occurrenceProbability/packetType->second.enhancedOccurrenceProbability);
	return N;
}

double MatrixTree::generateSequenceLikelihood(long sequenceLength, bool enhanced) {
	
	lastLogEnhancementWeight=0.0;
	
	double sum=0.0;
	
	for(map<long,Hit>::iterator hit=hits.begin();hit!=hits.end();hit++) {
		int nPackets=generateNumberOfPackets(sequenceLength,hit,enhanced);
		for(int i=0;i<nPackets;i++)
			sum+=generatePacketLikelihood(hit);
	}
	
	return sum;
}

/*void MatrixTree::initializeForEnhancedSimulation(long sequenceLength, double threshold, int iteration) {
	for(map<long,Hit>::iterator hit=hits.begin();hit!=hits.end();hit++) {
		double mean=0.0;
		for(int s=0;s<iteration;s++)
			mean+=generatePacketLikelihood(hit);
		mean/=iteration;
		hit->second.enhancedOccurrenceProbability=threshold/(sequenceLength*hits.size()*mean);
		cout<<hit->second.enhancedOccurrenceProbability/hit->second.occurrenceProbability<<endl;
		//hit->second.enhancedOccurrenceProbability=5*hit->second.occurrenceProbability;
	}
}*/

void MatrixTree::initializeForEnhancedSimulation(long sequenceLength, double threshold, int iteration) {
	double mean=0.0;
	for(int s=0;s<iteration;s++)
		mean+=generateSequenceLikelihood(sequenceLength,false);
	mean/=iteration;
	for(map<long,Hit>::iterator hit=hits.begin();hit!=hits.end();hit++)
		hit->second.enhancedOccurrenceProbability=hit->second.occurrenceProbability*threshold/mean;
	cout<<threshold/mean<<endl;
}