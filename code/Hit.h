#include <vector>
#include <map>

using namespace std;

class Hit {
	
public:
	
	typedef map<long,Hit>::iterator HitPointer;

	double likelihood;
	double occurrenceProbability; // probabilité d'occurrence d'un paquet !
	double enhancedOccurrenceProbability; // probabilité d'occurrence d'un paquet sous la loi d'échantillonnage préférentiel
	vector<pair<double, long> > overlaps; // table intermédiaire, permettant de remplir la suivante
	vector<pair<double, HitPointer> > overlapsPointers; // pointe sur le hit concerné dans la table map<long,Hit> de MatrixTree
	
	Hit(double likelihood, double occurrenceProbability) {
		this->occurrenceProbability=occurrenceProbability;
		this->likelihood=likelihood;
	}

	HitPointer descendInTree(double &packetLikelihood, double rand) {
		// La fonction ajoute d'abord l'adéquation du noeud/hit à l'adéquation du paquet
		// en cours de simulation.
		// Puis elle descend dans l'arbre (ou s'arrête) selon la valeur de rand (entre 0 et 1),
		// par recherche dichotomique dans overlaps.
		// Elle retourne hits.end() si elle s'arrête, un itérateur sur le noeud suivant sinon.
		
		packetLikelihood+=likelihood;
		int n=overlapsPointers.size();
		int min=0;
		int max=n-1;
		int middle;
		if(rand>overlapsPointers[max-1].first) return overlapsPointers[max].second; // pris en compte par la recherche dichotomique mais accélère grandement les choses, car il n'y a souvent pas de recouvrement
		if(rand<=overlapsPointers[min].first) return overlapsPointers[min].second; // cas non pris en compte par la recherche dichotomique
		while(max-min>1) {
			middle=(max+min)/2;
			if(rand>overlapsPointers[middle].first)
				min=middle;
			else
				max=middle;
		}
		return overlapsPointers[max].second;
	}

};

typedef map<long,Hit>::iterator HitPointer;