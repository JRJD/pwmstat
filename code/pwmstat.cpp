#include <iostream>
#include <fstream>
#include <exception>
#include <cstring>
#include <cmath>
#include <sstream>
#include <time.h>
#include "MatrixTree.h"
#include "readinput.h"
#include <cstddef>

using namespace std;

void displayHelp();
template<class T> void log(T data);

int main(int argc, char **argv) {
	
	//////////////////////////////////////////////////////////////////////////////////
	// Gestion des arguments
	char *sequenceFilename, *pwmFilename, *outputFilename, *csvFilename, *pwminfoFilename="temp";
	bool csvSet=false, sequenceSet=false, pwmSet=false, outputSet=false, nSet=false, pwminfoSet=false, m0Set=false;
	long nSimulations;
	int num_arg=1;
	while (num_arg < argc) {
		if ( (strcmp (argv[num_arg], "-m") == 0) && (num_arg + 1 < argc) ) {
			num_arg++;
			pwmFilename=argv[num_arg];
			num_arg++;
			pwmSet=true;
		} else if ( (strcmp (argv[num_arg], "-s") == 0) && (num_arg + 1 < argc) ) {
			num_arg++;
			sequenceFilename=argv[num_arg];
			num_arg++;
			sequenceSet=true;
		} else if ( (strcmp (argv[num_arg], "-o") == 0) && (num_arg + 1 < argc) ) {
			num_arg++;
			outputFilename=argv[num_arg];
			num_arg++;
			outputSet=true;
		} else if ( (strcmp (argv[num_arg], "-n") == 0) && (num_arg + 1 < argc) ) {
			num_arg++;
			nSimulations=atol(argv[num_arg]);
			num_arg++;
			nSet=true;
		} else if ( (strcmp (argv[num_arg], "-t") == 0) && (num_arg + 1 < argc) ) {
			num_arg++;
			csvFilename=argv[num_arg];
			num_arg++;
			csvSet=true;
		} else if ( (strcmp (argv[num_arg], "-a") == 0) && (num_arg + 1 < argc) ) {
			num_arg++;
			pwminfoFilename=argv[num_arg];
			num_arg++;
		} else if ( (strcmp (argv[num_arg], "-r") == 0) && (num_arg + 1 < argc) ) {
			num_arg++;
			pwminfoFilename=argv[num_arg];
			num_arg++;
			pwminfoSet=true;
		} else if ( strcmp(argv[num_arg], "-M0") == 0 ) {
			m0Set=true;
			num_arg++;
		} else {
			displayHelp();
			return 0;
		}
    }
	if( !(sequenceSet && pwmSet && outputSet && nSet) ) {
		displayHelp();
		return 0;
	}
	
	try {
		
		// classe représentant le modlèe choisi pour
		// la modélisation du génome (M0 ou M1).
		GenomeMarkovChain *genomeProbability;
		
		// adéquation le long du génome
		double likelihoodSum=0.0;
		
		// taille du génome
		int genomeLength=0;
		
		// taille de la matrice poids-position
		int pwmLength;
		
		// matrice poids-position
		vector< vector<double> > pwm;
		
		// fichier de sortie
		ofstream output(outputFilename);
		if (!output.is_open()) throw(string("Can't open output file."));
		output<<"# -----------------------------------------------------------------------------------"<<endl<<"#"<<endl;
		output<<"# \tP-value of positional weight matrix calculated by "<<"#"<<endl;
		output<<"# \tDumazert et al (2013) - JOBIM 2013 proceedings "<<endl<<"#"<<endl;
		output<<"# -----------------------------------------------------------------------------------"<<endl<<"#"<<endl<<"#"<<endl;
		
		
		/////////////////////////////////////////////////////////////////////////////////////////
		// Récupération de la matrice poids-position 
		// Ecriture dans le tableau pwm
		// On accède par exemple à la probabilité d'avoir un 'C' en 4e position avec pwm[3][1]
		log("Loading PWM.");
		readTransfacMatrix(pwmFilename, pwm);
		pwmLength=pwm.size();
		
		// détermination du nombre de mots
		int nWords=1;
		for(int j=0;j<pwmLength;j++) {
			int nNEqualZero=0;
			for(int i=0;i<4;i++)
				if(pwm[j][i]!=0.0)
					nNEqualZero++;
			nWords*=nNEqualZero;
		}
		
		// écriture de la matrice dans le fichier de sortie
		output<<"# ----> Positional weight matrix"<<endl<<"#"<<endl;
		output<<"# Loaded from file: "<<pwmFilename<<endl;
		output<<"# Length: "<<endl<<pwmLength<<endl;
		output<<"# Number of compatible words: "<<endl<<nWords<<endl;
		
		
		// création des puissances de 4
		setPower(pwmLength);
		
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////
		// Lecture du génome
		
		if(m0Set) {
			log("Analyzing sequence (Markov 0 model).");
			readGenomeM0(sequenceFilename, pwm, likelihoodSum, genomeLength, genomeProbability);
			output<<"#"<<endl<<"# ----> Sequence analysis (Markov model of order 0)"<<endl<<"#"<<endl;
			output<<"# Sequence length: "<<endl<<genomeLength<<endl;
			output<<"# Letter frequency (A,C,G,T): "<<endl;
			for(int i=0;i<4;i++) output<<genomeProbability->marginal(i)<<endl;
		} else {
			log("Analyzing sequence (Markov 1 model).");
			readGenomeM1(sequenceFilename, pwm, likelihoodSum, genomeLength, genomeProbability);
			output<<"#"<<endl<<"# ----> Sequence analysis (Markov model of order 1)"<<endl<<"#"<<endl;
			output<<"# Sequence length: "<<endl<<genomeLength<<endl;
			output<<"# Oriented pair of letters frequency: "<<endl;
			output<<"\tA\t\tC\t\tG\t\tT"<<endl;
			for(int i=0;i<4;i++) {
				output<<int_to_letter(i);
				for(int j=0;j<4;j++) {
					output<<"\t"<<genomeProbability->joint(i, j);
				}
				output<<endl;
			}
		}
		output<<"# Weighted count: "<<endl<<likelihoodSum<<endl;
		
		//////////////////////////////////////////////////////////////////////////////////////
		// Approximation gaussienne
		
		double mean=1.0;
		double variance=0.0;
		vector<double> cov;
		
		// Calcul de la moyenne
		for(int j=0;j<pwmLength;j++) {
			double w=0.0;
			for(int i=0;i<4;i++)
				w+=genomeProbability->marginal(i)*pwm[j][i];
			mean*=w;
		}
		
		// Calcul des covariances
		for(int d=0;d<pwmLength;d++) {
			cov.push_back(1.0);
			// avant overlap
			for(int j=0;j<d;j++) {
				double w=0.0;
				for(int i=0;i<4;i++)
					w+=genomeProbability->marginal(i)*pwm[j][i];
				cov[d]*=w;
			}
			// pendant overlap
			for(int j=d;j<pwmLength;j++) {
				double w=0.0;
				for(int i=0;i<4;i++)
					w+=genomeProbability->marginal(i)*pwm[j][i]*pwm[j-d][i];
				cov[d]*=w;
			}
			// après overlap
			for(int j=pwmLength-d;j<pwmLength;j++) {
				double w=0.0;
				for(int i=0;i<4;i++)
					w+=genomeProbability->marginal(i)*pwm[j][i];
				cov[d]*=w;
			}
		}
		
		// Calcul de la variance
		variance+=cov[0]-(2*pwmLength-1)*mean*mean;
		for(int j=1;j<pwmLength;j++)
			variance+=2*cov[j];
		
		// Passage à T
		mean*=genomeLength-pwmLength+1;
		variance*=genomeLength-pwmLength+1;
		double gaussianPvalue=1.0-cdf((likelihoodSum-mean)/sqrt(variance));
		
		// écriture dans la console
		stringstream sss;
		sss<<"Gaussian approximation:"<<endl;
		sss<<"\tExpectation: "<<mean<<endl;
		sss<<"\tVariance: "<<variance<<endl;
		sss<<"\tP-value: "<<gaussianPvalue;
		log(sss.str());
		
		// écriture dans le fichier de sortie
		output<<"#"<<endl<<"# ----> Gaussian approximation"<<endl<<"#"<<endl;
		output<<"# Expectation: "<<endl<<mean<<endl;
		output<<"# Variance: "<<endl<<variance<<endl;
		output<<"# P-value: "<<endl<<gaussianPvalue<<endl;
		
		
		
		if(!pwminfoSet) {
			// On ne fait pas de prétraitement si l'utilisateur spécifie un fichier de prétraitement
		
			//////////////////////////////////////////////////////////////////////////////////////////////
			// Etude des mots et des recouvrements
			
			// On va recenser tous les cas de bases (hits)
			// On crée une map (une sorte de dictionnaire) dont la clé est le cas de base sous forme d'entier
			// et la valeur l'adéquation
			
			map<long,double> hits;
			
			
			// A partir de maintenant, les séquences sont vues comme des entiers (long) 
			// dont la représentation en base 4 donne la séquence en paires de bases,
			// ou comme des vector<int> avec A=0, C=1, G=2 et T=3.
			
			
			
			// Récurrence permettant d'énumérer les cas de bases par l'écriture en base 4 de séquence
			// On commence par insérer tous les cas de base dans le dictionnaire hits
			log("Computing words.");
			recHits(hits,pwm,pwmLength,0,1.0,0);
			
			// On va maintenant calculer tous les fils (recouvrements) pour un cas de base donné,
			// et les recouvrements par la droite pour obtenir la probabilité d'apparition d'un paquet
			// Dans cette version, pour chaque hit on étudie dans l'ordre des recouvrements : 
			// on vérifie que le décalage permet d'obtenir un hit
			// si oui, on lance une récurrence qui complète le bout de mot pour créer tous les hits possibles
			// et rien que les hits, contrairement à précédemment
			// puis on vérifie que la séquence ne contient pas de hits d'ordre inférieur
			// L'idée globale est qu'il vaut mieux tester beaucoup de fois peu de séquences que tester
			// moins de fois trop de séquences. Le gain est significatif.
			log("Computing overlaps between words.");
			long counter=0;
			ofstream temp(pwminfoFilename);
			if (!temp.is_open()) throw(string("Can't open temporary file for storing data about PWM."));
			temp<<pwmLength<<endl;
			clock_t beginning=clock();
			for(map<long,double>::iterator hit=hits.begin();hit!=hits.end();hit++) {
				counter++;
				if(counter==10 || counter%1000==0) {
					clock_t now=clock();
					double d=(double)(now-beginning)/CLOCKS_PER_SEC;
					double remaining=(double_t)(d*(((double)nWords)/counter))-d;
					stringstream ss;
					ss<<"Computing overlaps of word "<<counter<<"/"<<hits.size()<<endl;
					ss<<"\tEstimated remaining time: "<<remaining<<" s";
					log(ss.str());
				}
				
				HitInformation hitInfo(hit->second,map<long,double>(),0.0);
				double probabilitySum=0.0;		// Sert au calcul de occurrenceProbability
				long hitSequence=hit->first;
				for(int order=1;order<pwmLength;order++) {
					// Recouvrements par la droite
					long wordBeginning=hitSequence/power[order];
					if(isPartialHit(pwm,0,pwmLength-order,wordBeginning))
						recRightOverlaps(pwm,pwmLength,genomeProbability,hitInfo,hitSequence,order,wordBeginning,1.0,pwmLength-order);
					// Recouvrements par la gauche
					long wordEnding=hitSequence%power[order];
					if(isPartialHit(pwm,pwmLength-order,pwmLength,wordEnding))
						recLeftOverlaps(probabilitySum,pwm,pwmLength,genomeProbability,hitSequence,order,wordEnding,1.0,pwmLength-order-1);
				}
				hitInfo.occurrenceProbability=1.0-probabilitySum;									//
				long hitCopy=hitSequence;															//
				hitInfo.occurrenceProbability*=genomeProbability->marginal(hitCopy%4);				//
				for(int i=1;i<pwmLength;i++) {														//
					int lastLetter=hitCopy%4;														//
					hitCopy/=4;																		//
					hitInfo.occurrenceProbability*=genomeProbability->forwardConditional(lastLetter, hitCopy%4);	// On ajoute l'information de occurrenceProbability
				}																					//
				
				// écriture
				temp<<"+ "<<hitSequence<<" ; "<<hitInfo.likelihood<<" ; "<<hitInfo.occurrenceProbability<<endl;
				for(map<long,double>::iterator son=hitInfo.overlaps.begin();son!=hitInfo.overlaps.end();son++) {
					temp<<"- "<<son->first<<" ; "<<son->second<<endl;
				}
			}
			temp.close();
			hits.clear();
			
		}
		
		
		
		//////////////////////////////////////////////////////////////////////////////////////
		// Simulation
		
		log("Loading PWM information file.");
		
		ifstream pwminfo(pwminfoFilename);
		if(!pwminfo.is_open()) throw(string("Can't open PWM information file."));
		MatrixTree tree(pwminfo);
		pwminfo.close();
		
		int nPositives=0;
		
		log("Beginning simulation.");
		
		ofstream csv;
		if(csvSet) {
			csv.open(csvFilename);
			if (!csv.is_open()) throw(string("Can't open csv file."));
		}
		clock_t beginning=clock();
		for(long s=0;s<nSimulations;s++) {
			// écriture du résultat
			double likelihood=tree.generateSequenceLikelihood(genomeLength);
			if(csvSet) csv<<likelihood<<endl;
			if(likelihood>likelihoodSum) nPositives++;
			// affichage de l'avancement
			if(s+1==1000 || (s+1)%100000==0) {
				clock_t now=clock();
				double d=(double)(now-beginning)/CLOCKS_PER_SEC;
				double remaining=(double_t)(d*(((double)nSimulations)/(s+1)))-d;
				stringstream ss;
				ss<<"Simulation "<<s+1<<"/"<<nSimulations<<endl;
				ss<<"\tP-value calculated so far: "<<nPositives/((double)(s+1))<<endl;
				if(nPositives==0)
					ss<<"\tThus P-value < "<<3.84/(s+1)<<" at 95%"<<endl;
				else {
					double est=nPositives/((double)(s+1));
					double error=1.96*sqrt(est*(1-est)/(s+1));
					ss<<"\t95% confidence interval: +/- "<<error<<endl;
				}
				ss<<"\tEstimated remaining time: "<<remaining<<" s";
				log(ss.str());
			}
		}
		stringstream ss;
		ss<<"End of simulation."<<endl;
		ss<<"\tP-value: "<<nPositives/((double)nSimulations)<<endl;
		ss<<"\tNumber of simulations: "<<nSimulations<<endl;
		if(nPositives==0)
			ss<<"\tThus P-value < "<<3.84/nSimulations<<" at 95%";
		else {
			double est=nPositives/((double)nSimulations);
			double error=1.96*sqrt(est*(1-est)/nSimulations);
			ss<<"\t95% confidence interval: +/- "<<error;
		}
		log(ss.str());
		csv.close();
		
		// écriture du résultat
		output<<"#"<<endl<<"# ----> compound Poisson approximation"<<endl<<"#"<<endl;
		output<<"# Number of simulations: "<<endl<<nSimulations<<endl;
		output<<"# P-value: "<<endl<<nPositives/((double)nSimulations)<<endl;
		if(nPositives==0)
			output<<"# Thus P-value inferior to "<<endl<<3.84/nSimulations<<endl<<"# at 95%"<<endl;
		else {
			double est=nPositives/((double)nSimulations);
			double error=1.96*sqrt(est*(1-est)/nSimulations);
			output<<"# 95% confidence interval inferior bound:"<<endl<<est-error<<endl;
			output<<"# 95% confidence interval inferior bound:"<<endl<<est+error<<endl;
		}
		output<<"#"<<endl<<"# This means that:"<<endl;
		output<<"# the probability that the motif (pwm) gets a weighted count "<<endl;
		output<<"# along a random sequence of letters (with probabilities specified above)"<<endl;
		output<<"# of length "<<genomeLength<<" greater or equal to the observed count "<<likelihoodSum<<" is of "<<nPositives/((double)nSimulations)<<"."<<endl<<"#"<<endl;
		output<<"# It is also a measurement of the exceptionality of the motif:"<<endl;
		output<<"# the probability that the motif be more \"frequent\""<<endl;
		output<<"# along a random sequence (as specified above) than along the input sequence"<<endl;
		output<<"# is of "<<nPositives/((double)nSimulations)<<"."<<endl<<"#"<<endl;
		if(csvSet) {
			output<<"# File "<<csvFilename<<" contains the simulated values of the weighted count."<<endl;
			output<<"# Display values in an histogram to visualize the weighted count distribution."<<endl;
		}
		
		// fermeture du fichier de sortie
		output.close();
		
	}
	// Différents types d'erreur traitées
	catch (exception &e) {
		cout<<e.what()<<endl;
		displayHelp();
	}
	catch (string &s) {
		cout<<s<<endl;
		displayHelp();
	}
	catch (...) {
		cout<<"Unknown error."<<endl;
		displayHelp();
	}
	
	return EXIT_SUCCESS;
}

void displayHelp() {
	cout<<endl<<"PWMstat:"<<endl;
	cout<<endl<<"\tComputes for a given PWM and a DNA sequence,"<<endl;
	cout<<"\tthe P-value associated with the PWM,"<<endl;
	cout<<"\ti.e. the probability that the weighted count along a \"random\" sequence"<<endl;
	cout<<"\tgenerated under a Markovian model of order 1 derived from the input sequence"<<endl;
	cout<<"\tbe superior to the weighted count observed in the input sequence."<<endl;
	cout<<endl<<"\tSyntax: pwmstat[-t <filename>] [-a <filename>] [-r <filename>] [-h] [-M0] -m <filename> -s <filename> -n <int> -o <filename>"<<endl;
	cout<<endl<<"\tArguments:"<<endl;
	cout<<"\t\t-m PWM file"<<endl;
	cout<<"\t\t-s Genome file (FASTA format)"<<endl;
	cout<<"\t\t-n Number of simulations"<<endl;
	cout<<"\t\t-o Output file"<<endl;
	cout<<endl<<"\tOptions:"<<endl;
	cout<<"\t\t-h  Display help"<<endl;
	cout<<"\t\t-M0 Use a Markov model of order 0 instead of order 1"<<endl;
	cout<<"\t\t-a  Specify a name different from \"temp\" for pre-treatment file (for later re-use)"<<endl;
	cout<<"\t\t-r  Re-use a pre-treatment file (pre-treatment can be long)"<<endl;
	cout<<"\t\t-t  Save simulation values"<<endl<<endl;
}

template<class T> void log(T data) {
	time_t *t=new time_t;
	*t=time(0);
	cout<<asctime(localtime(t));
	cout<<"\t"<<data<<endl<<endl;
	delete t;
}
